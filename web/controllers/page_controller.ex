defmodule WildflowerLanding.PageController do
  use WildflowerLanding.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
